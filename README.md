# Serpent

Tento projekt bol inšpirovaný videotutoriálom of [@Yablka](https://www.youtube.com/watch?v=D-dtyO44ANA&t=3944s&ab_channel=ROBWEBsyablkom)

## Použité technológie

Projekt je postavený v Reacte a obsahuje Material UI dialog

## Stručný popis

Hracia plocha, had a jedlo sú štylizované divy.
Dokument zachytáva stláčanie šípok a na základe toho mení smer pohybu hada.
Had je reprezentovaný poľom objektov, každý objekt predstavuje jeden segment hada a obsahuje jeho súradnice v rámci hracej plochy.
Pohyb hada a jeho rast sú programované pomocou operácií s poľom. 
Pohyb proti smeru aktuálneho pohybu má rovnaký efekt ako prienik medzi hlavou a telom hada - prehra.

Ide o tretiu verziu tejto hry, v každej verzii som k problému pristupoval iným spôsobom, iba tento bol správny.

UPDATE: Podarilo sa mi do hry integrovať Tensorflow.js Handpose recognition a definovať rôzne gestá (ukazovát doľava/doprava, palec nahor/nadol) a každé z nich mení smerovanie hada, funkcionalita šípok zostala nezmenená.
Otvorená dlaň je neutrálnym gestom a funguje ako príprava na smerodajné gesto - gestá samotné nemajú dokonalú definíciu a akékoľvek iné gesto (okrem otvorenej dlane) môže byť rozoznané ako jedno zo smerodajných giest

Nájdete tu: https://marekzska.itervitae.eu/HandSerpent/ - dajte funkcionalite pár sekúnd na načítanie