import {
  Finger,
  FingerCurl,
  FingerDirection,
  GestureDescription,
} from "fingerpose";

const pointRightDescription = new GestureDescription("pointRight")

pointRightDescription.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0);
pointRightDescription.addDirection(Finger.Index, FingerDirection.HorizontalRight, 1.0);
pointRightDescription.addDirection(
  Finger.Index,
  FingerDirection.DiagonalUpRight,
  0.9
);
pointRightDescription.addDirection(
  Finger.Index,
  FingerDirection.DiagonalDownRight,
  0.9
);

for (let finger of [Finger.Thumb, Finger.Middle, Finger.Ring, Finger.Pinky]) {
  pointRightDescription.addCurl(finger, FingerCurl.FullCurl, 1.0);
  pointRightDescription.addCurl(finger, FingerCurl.HalfCurl, 0.25);
}

pointRightDescription.addDirection(
  Finger.Thumb,
  FingerDirection.DiagonalUpRight,
  1.0
);
pointRightDescription.addDirection(
  Finger.Thumb,
  FingerDirection.VerticalUp,
  1.0
);
pointRightDescription.addDirection(
  Finger.Thumb,
  FingerDirection.HorizontalRight,
  1.0
);

export default pointRightDescription;