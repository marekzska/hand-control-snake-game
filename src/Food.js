import React from "react";

export default function Food(props){
    const style = {
        left: `${props.food.x}%`,
        top: `${props.food.y}%`
    }
    return (
        <div className="food" style={style}></div>
    )
}