import React, { useRef, useEffect } from "react";
import * as tf from "@tensorflow/tfjs";
import * as handpose from "@tensorflow-models/handpose";
import * as fp from "fingerpose";
import Webcam from "react-webcam";
import { drawHand } from "./resources/utilities";
import pointRightDescription from "./gestures/pointRight";
import pointLeftDescription from "./gestures/pointLeft";
import thumbsUpDescription from "./gestures/thumbUp";
import thumbsDownDescription from "./gestures/thumbDown";

function HandRecognition(props) {
  const webcamRef = useRef(null);
  const canvasRef = useRef(null);
  let clear = useRef(null);
  useEffect(() => {
    const runHandpose = async () => {
      const net = await handpose.load();
      console.log("Handpose model loaded.");
      clear.current = setInterval(() => {
        detect(net);
      }, 100);
    };

    const detect = async (net) => {
      if (
        typeof webcamRef.current !== "undefined" &&
        webcamRef.current !== null &&
        webcamRef.current.video.readyState === 4
      ) {
        const video = webcamRef.current.video;
        const videoWidth = webcamRef.current.video.videoWidth;
        const videoHeight = webcamRef.current.video.videoHeight;

        webcamRef.current.video.width = videoWidth;
        webcamRef.current.video.height = videoHeight;

        canvasRef.current.width = videoWidth;
        canvasRef.current.height = videoHeight;

        const hand = await net.estimateHands(video);
        // console.log(hand);

        if (hand.length > 0) {
          const GE = new fp.GestureEstimator([
            pointRightDescription,
            pointLeftDescription,
            thumbsUpDescription,
            thumbsDownDescription,
          ]);

          const gesture = await GE.estimate(hand[0].landmarks, 8);
          if (gesture.gestures.length > 0 && gesture.gestures !== undefined) {
            const confidence = gesture.gestures.map(
              (prediction) => prediction.score
            );
            const maxConfidence = confidence.indexOf(
              Math.max.apply(null, confidence)
            );
            console.log(gesture.gestures[maxConfidence].name);
            let direction = gesture.gestures[maxConfidence].name;
              if(direction === "thumbUp"){props.onChangeDirection("Up")}
              if(direction === "thumbDown"){props.onChangeDirection("Down")}
              // bug caused by the webcam mirroring
              if(direction === "pointLeft"){props.onChangeDirection("Right")}
              if(direction === "pointRight"){props.onChangeDirection("Left")}
            // console.log(gesture)
          }
        }

        const ctx = canvasRef.current.getContext("2d");
        ctx.translate(videoWidth, 0);
        ctx.scale(-1, 1);
        drawHand(hand, ctx);
      }
    };

    runHandpose();
    return () => clearInterval(clear.current);
  }, []);

  return (
    <div className='WebCam'>
      <Webcam
        mirrored={true}
        ref={webcamRef}
        style={{
          position: "absolute",
          marginLeft: "auto",
          marginRight: "auto",
          left: 0,
          right: 0,
          textAlign: "center",
          zIndex: 9,
          width: 340,
          height: 180,
        }}
      />
      <canvas
        ref={canvasRef}
        style={{
          position: "absolute",
          marginLeft: "auto",
          marginRight: "auto",
          left: 0,
          right: 0,
          textAlign: "center",
          zIndex: 9,
          width: 340,
          height: 180,
        }}
      />
    </div>
  );
}

export default HandRecognition;
