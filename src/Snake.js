import React from "react";

export default function Snake(props) {
  return (
    <>
      {props.snake.map((dot, index) => {
        const style = {
          left: `${dot.x}%`,
          top: `${dot.y}%`,
        };
        return (
            <div className="snakeDot" key={index} style={style}></div>
        )
      })}
    </>
  );
}
